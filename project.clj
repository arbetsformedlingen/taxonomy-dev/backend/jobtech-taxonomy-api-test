(defproject jobtech-taxonomy-api-test "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies
  [[org.clojure/clojure "1.10.1"]
   [clj-http "3.10.2"]
   [cheshire "5.10.0"]
   [martian "0.1.14"]
   [martian-clj-http "0.1.14"]
   [io.xapix/paos "0.2.4"]]
  :repositories [["enonic" "https://repo.enonic.com/public/"]]
  :main ^:skip-aot jobtech-taxonomy-api-test.core
  :test-paths ["test/clj"]
  :target-path "target/%s"

  :plugins [[lein-cljfmt "0.6.6"]
            [lein-ancient "0.6.15"]
            [lein-nsorg "0.3.0"]
            [jonase/eastwood "0.3.6"]]

  :eastwood {:exclude-linters [:constant-test]}
  :cljfmt {}

  :profiles {:uberjar {:aot :all}
             :kaocha {:dependencies [[lambdaisland/kaocha "1.0.672"]]}}
  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
