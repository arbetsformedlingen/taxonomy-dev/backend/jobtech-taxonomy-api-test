(ns ^:integration-smoke-tests  jobtech-taxonomy-api-test.test.graphql-test
  (:require
   [clj-http.client :as client]
   [clojure.test :refer [deftest is testing]]
   [jobtech-taxonomy-api-test.test.config :as config]))

(def graphql-base-url (str config/base-url "v1/taxonomy/graphql"))

(defn call-api [graphql-query]
  (:body (client/get graphql-base-url {:as :json-strict
                                       :headers {"api-key" config/api-key
                                                 "Content-Type" "application/json"}
                                       :debug false
                                       :query-params {"query" graphql-query}})))

(defn count-result [query]
  (count (:concepts (:data (call-api query)))))

(def query-skill-with-headline
  "query skillWithHeadline { concepts ( version: \"1\", type: \"skill\")
    {
    preferred_label
    id
    deprecated_legacy_id
    broader{
      definition
    }}}")

(defn test-query-skill-with-headline []
  (is (< 20 (count-result query-skill-with-headline))))

(def query-driving-licence
  "query driving {
  concepts (type:\"driving-licence\"){
    type
    preferred_label
    id
    short_description
   }
  }
  ")

(defn test-query-driving-licence []
  (is (< 10 (count-result query-driving-licence))))

(def query-mjukvaruutveklare-skills
  "query testSkills {
  concepts(id: \"DJh5_yyF_hEM\") {
    id
    type
    preferred_label
    related(type: \"skill\") {
      id
      preferred_label
      type
    }
  }
}
")

(defn test-query-mjukvaruutveklare-skills []
  (is (< 0 (count-result query-mjukvaruutveklare-skills))))

;; This one fails!


(def killer-query
  "query MyQuery {
  concepts(type: \"occupation-field\") {
    id
    type
    preferred_label
    narrower(type: \"ssyk-level-4\") {
      id
      preferred_label
      type
      narrower(type: \"occupation-name\") {
        id
        preferred_label
        type
        substitutes{
          id
          preferred_label
          type
          related(type: \"keyword\"){
            id
            preferred_label
            type
          }
        }
      }
    }
  }
}
")

(deftest smoke-test-graphql
  (testing "Smoke test Graphql queries"
    (test-query-driving-licence)
    (test-query-skill-with-headline)
    (test-query-mjukvaruutveklare-skills)))
